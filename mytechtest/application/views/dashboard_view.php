<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Dashboard</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/components.css">
</head>

<!-- Modal Add -->
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Add Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('Dashboard/AddData') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="nama" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="alamat" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="telp" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Gender</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="Male" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                Male
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="Female">
                            <label class="form-check-label" for="exampleRadios2">
                                Female
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Card Number</label>
                        <input type="text" name="nomor_ktp" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Picture Card</label>
                        <input type="file" name="foto_ktp" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Picture Profile</label>
                        <input type="file" name="foto_profile" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Status</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="exampleRadios3" value=1 checked>
                            <label class="form-check-label" for="exampleRadios3">
                                Actice
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="exampleRadios4" value=0>
                            <label class="form-check-label" for="exampleRadios4">
                                Not active
                            </label>
                        </div>
                    </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Update -->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalUpdate">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Update Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('Dashboard/UpdateDataText') ?>" method="post">
                    <input type="hidden" id="code" name="code">
                    <div class="form-group">
                        <label>Id Employee</label>
                        <input type="text" id="id_karyawan" name="id_karyawan" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="nama" id="nama" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="alamat" id="alamat" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="telp" id="telp" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Gender</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="male" value="Male">
                            <label class="form-check-label" for="exampleRadios1">
                                Male
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="female" value="Female">
                            <label class="form-check-label" for="exampleRadios2">
                                Female
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Card Number</label>
                        <input type="text" name="nomor_ktp" id="nomor_ktp" class="form-control" required>
                    </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Update Status -->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalSetStatus">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Set Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('Dashboard/SetStatus') ?>" method="post">
                    <input type="hidden" id="codeSetStatus" name="code">
                    <div class="form-group">
                        <label class="d-block">Status</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="active" value=1>
                            <label class="form-check-label">
                                Active
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="NotActive" value=0>
                            <label class="form-check-label">
                                Not Active
                            </label>
                        </div>
                    </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal view foto ktp -->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalViewFotoKTP">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="view1-tab" data-toggle="tab" href="#view1" role="tab" aria-controls="view1" aria-selected="true">View Photo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="view2-tab" data-toggle="tab" href="#view2" role="tab" aria-controls="view2" aria-selected="false">Update Photo</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="view1" role="tabpanel" aria-labelledby="view1-tab">
                            <article class="article article-style-c">
                                <div class="article-header">
                                    <img class="article-image" id="foto_ktp"></img>
                                </div>
                            </article>
                        </div>
                        <div class="tab-pane fade" id="view2" role="tabpanel" aria-labelledby="view2-tab">
                            <div class="card">
                                <div class="card-body">
                                    <form action="<?php echo base_url('Dashboard/UpdateFotoKTP') ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Picture Card</label>
                                            <input type="hidden" id="code1" name="code">
                                            <input type="hidden" id="oldFotoKTP" name="oldFotoKTP">
                                            <input type="file" name="foto_ktp" class="form-control" required>
                                        </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal view foto Profile -->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalViewFotoProfile">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
                <h5 class="modal-title">View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="view3-tab" data-toggle="tab" href="#view3" role="tab" aria-controls="view3" aria-selected="true">View Photo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="view4-tab" data-toggle="tab" href="#view4" role="tab" aria-controls="view4" aria-selected="false">Update Photo</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="view3" role="tabpanel" aria-labelledby="view3-tab">
                                <article class="article article-style-c">
                                    <div class="article-header">
                                        <img class="article-image" id="foto_profile"></img>
                                    </div>
                                </article>
                            </div>
                            <div class="tab-pane fade" id="view4" role="tabpanel" aria-labelledby="view4-tab">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="<?php echo base_url('Dashboard/UpdateFotoProfile') ?>" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Picture Card</label>
                                                <input type="hidden" id="code2" name="code">
                                                <input type="hidden" id="oldFotoProfile" name="oldFotoProfile">
                                                <input type="file" name="foto_profile" class="form-control" required>
                                            </div>
                                    </div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                        <button class="btn btn-secondary" type="reset">Reset</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<body class="layout-3">
    <div id="app">
        <div class="main-wrapper container">
            <!-- <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <a href="index.html" class="navbar-brand sidebar-gone-hide">Tech assessment</a>
                <div class="navbar-nav">
                    <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
                </div>
            </nav> -->

            <nav class="navbar navbar-secondary navbar-expand-lg" style="margin-top:-5em">
                <div class="container">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a href="<?php echo base_url() ?>" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
    
            <!-- Sweet alert -->
            <div id="flash" data-flash="<?= $this->session->flashdata('addFlash') ?>"></div>
            <div id="flash1" data-flash1="<?= $this->session->flashdata('cekIdFlash') ?>"></div>
            <div id="flash2" data-flash2="<?= $this->session->flashdata('updateFlash') ?>"></div>
            <div id="flash3" data-flash3="<?= $this->session->flashdata('SetStatusFlash') ?>"></div>
            <div id="flash4" data-flash4="<?= $this->session->flashdata('errUploadFotoFlash') ?>"></div>
            <div id="flash5" data-flash5="<?= $this->session->flashdata('updateFotoFlash') ?>"></div>
            
            <!-- Main Content -->
            <div class="main-content" style="margin-top:-6em">
                <section class="section">
                    <div class="section-body">
                        <h2 class="section-title">Data employee</h2>
                        <!-- <p class="section-lead">This page is for CRUD employee data.</p> -->

                        <div class="card">
                            <div class="card-body">

                                <div class="buttons">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Data</button>
                                    <button class="btn btn-primary" id="ButtonModalUpdate">Edit Data</button>
                                    <button class="btn btn-primary" id="ButtonModalSetStatus">Set Status</button>
                                    <button class="btn btn-primary" id="modal-6">Download Excel</button>
                                </div>
                                <hr/>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body p-0">
                                                <div class="table-responsive">

                                                    <table class="table table-striped" id="listEmployee">
                                                        <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Id Karyawan</th>
                                                            <th>Nama</th>
                                                            <th>Alamat</th>
                                                            <th>Telp</th>
                                                            <th>Gender</th>
                                                            <th>Nomor KTP</th>
                                                            <th>Foto KTP</th>
                                                            <th>Foto Profile</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            foreach($dt_emp as $row){
                                                                $nama = str_replace(' ','_',$row->nama);
                                                                $alamat = str_replace(' ','_',$row->alamat);
                                                        ?>
                                                        <tr>
                                                            <td><input type="checkbox" class="Kode" value="<?php echo $row->code?>" data-id_karyawan=<?php echo $row->id_karyawan ?> data-nama=<?php echo $nama ?> data-alamat=<?php echo $alamat ?> data-telp=<?php echo $row->telp ?> data-gender=<?php echo $row->gender ?> data-nomor_ktp=<?php echo $row->nomor_ktp ?> data-foto_ktp=<?php echo $row->foto_ktp ?> data-foto_profile=<?php echo $row->foto_profile ?> data-status=<?php echo $row->status ?>></td>
                                                            <td><?php echo $row->id_karyawan?></td>
                                                            <td class="nama"><?php echo $row->nama?></td>
                                                            <td><?php echo $row->alamat?></td>
                                                            <td><?php echo $row->telp?></td>
                                                            <td><?php echo $row->gender?></td>
                                                            <td><?php echo $row->nomor_ktp?></td>
                                                            <td>
                                                                <a href="javascript:;" data-foto_ktp="<?php echo $row->foto_ktp?>" data-code="<?php echo $row->code ?>" data-toggle="modal" data-target="#ModalViewFotoKTP"> 
                                                                    <button class="btn btn-secondary">View</button>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:;" data-foto_profile="<?php echo $row->foto_profile?>" data-code="<?php echo $row->code ?>" data-toggle="modal" data-target="#ModalViewFotoProfile">
                                                                    <button class="btn btn-secondary">View</button>
                                                                </a>
                                                            </td>
                                                            <?php
                                                                if($row->status == 1){
                                                            ?>
                                                            <td>
                                                                <div class="badge badge-success">Active</div>
                                                            </td>
                                                            <?php
                                                                } else {
                                                            ?>
                                                                <td>
                                                                <div class="badge badge-danger">Not Active</div>
                                                                </td>
                                                            <?php
                                                                }
                                                            ?>
                                                        </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

            </div>
            </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
        Copyright &copy; 2021
        </div>
      </footer>
    </div>
  </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?=base_url()?>assets/js/stisla.js"></script>

    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>

    <!-- Template JS File -->
    <script src="<?=base_url()?>assets/js/scripts.js"></script>
    <script src="<?=base_url()?>assets/js/custom.js"></script>

    <!-- Sweet alert -->
    <script src="<?=base_url()?>assets/dist/sweetalert2.all.min.js"></script>
    <script>
        const flash = $('#flash').data('flash');
        const flash1 = $('#flash1').data('flash1');
        const flash2 = $('#flash2').data('flash2');
        const flash3 = $('#flash3').data('flash3');
        const flash4 = $('#flash4').data('flash4');
        const flash5 = $('#flash5').data('flash5');
        
        if(flash){
            Swal.fire({
                icon: 'success',
                title: 'Success add data employee',
            })
        }
        if(flash1){
            Swal.fire({
                icon: 'warning',
                title: 'Data Id Card already exist',
            })
        }
        if(flash2){
            Swal.fire({
                icon: 'success',
                title: 'Success Update data',
            })
        }
        if(flash3){
            Swal.fire({
                icon: 'success',
                title: 'Set Status success',
            })
        }
        if(flash4){
            Swal.fire({
                icon: 'warning',
                title: 'The photo format must be gif,jpg,png and jpeg',
            })
        }
        if(flash5){
            Swal.fire({
                icon: 'success',
                title: 'Update photo success',
            })
        }

        <?php $this->session->sess_destroy(); ?>
    </script>

    <!-- Modal Update -->
    <script>
        $(document).ready(function() {
            //Pager cekbox
            $("#ButtonModalUpdate").click(function() {
                const code = [];
                $.each($("input[class='Kode']:checked"), function(){
                    code.push($(this).val());
                });

                if(code.length === 0){
                    $("#ModalUpdate").modal("hide");
                    Swal.fire({
                        icon: 'warning',
                        title: 'Please select one data',
                    });
                } else if(code.length === 1){
                    $("#ModalUpdate").modal("show");
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: `Please don't select more than one data`,
                    });
                }
            });

            $('#ModalUpdate').on('show.bs.modal', function (event) {
                //  store current modal reference
                var modal = $(this);

                const code = $('input[class="Kode"]:checked').val();
                const id_karyawan = $('input[class="Kode"]:checked').data('id_karyawan');
                const nama = $('input[class="Kode"]:checked').data('nama');
                const alamat = $('input[class="Kode"]:checked').data('alamat');
                const telp = $('input[class="Kode"]:checked').data('telp');
                const nomor_ktp = $('input[class="Kode"]:checked').data('nomor_ktp');
                const gender = $('input[class="Kode"]:checked').data('gender');

                const replaceNama = nama.replace(/_/g, ' ');
                const replaceAlamat = alamat.replace(/_/g, ' ');

                modal.find('#code').attr("value", code);
                modal.find('#id_karyawan').attr("value", id_karyawan);
                modal.find('#nama').attr("value", replaceNama);
                modal.find('#alamat').html(replaceAlamat);
                modal.find('#telp').attr("value", telp);
                if(gender === 'Male'){
                    modal.find('#male').prop("checked", true);
                }else {
                    modal.find('#female').prop("checked", true);
                }
                modal.find('#nomor_ktp').attr("value", nomor_ktp);
                
            });

        });
    </script>

    <!-- Modal Set Status -->
    <script>
        //Pager cekbox
        $("#ButtonModalSetStatus").click(function() {
            const code = [];
            $.each($("input[class='Kode']:checked"), function(){
                code.push($(this).val());
            });

            if(code.length === 0){
                $("#ModalSetStatus").modal("hide");
                Swal.fire({
                    icon: 'warning',
                    title: 'Please select one data',
                });
            } else if(code.length === 1){
                $("#ModalSetStatus").modal("show");
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: `Please don't select more than one data`,
                });
            }
        });

        $('#ModalSetStatus').on('show.bs.modal', function (event) {
                //  store current modal reference
                var modal = $(this);

                const code = $('input[class="Kode"]:checked').val();
                const status = $('input[class="Kode"]:checked').data('status');

                modal.find('#codeSetStatus').attr("value", code);
                if(status === 1){
                    modal.find('#active').prop("checked", true);
                }else {
                    modal.find('#NotActive').prop("checked", true);
                }
                
            });
    </script>

    <!-- datables -->
    <script>
        $(document).ready(function() {
            $('#listEmployee').DataTable({});
        });
    </script>

    <!-- Modal view picture -->
    <script>
        $(document).ready(function() {
            // Untuk sunting
            $('#ModalViewFotoKTP').on('show.bs.modal', function (event) {
                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
                var modal = $(this);

                // Isi nilai pada field
                modal.find('#code1').attr("value", div.data('code'));
                modal.find('#oldFotoKTP').attr("value", div.data('foto_ktp'));
                modal.find('#foto_ktp').attr("src", `<?=base_url()?>uploads/${div.data('foto_ktp')}`);
            });

            $('#ModalViewFotoProfile').on('show.bs.modal', function (event) {
                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
                var modal = $(this)

                // Isi nilai pada field
                modal.find('#code2').attr("value", div.data('code'));
                modal.find('#oldFotoProfile').attr("value", div.data('foto_profile'));
                modal.find('#foto_profile').attr("src", `<?=base_url()?>uploads/${div.data('foto_profile')}`);
            });
        });
    </script>

</body>

</html>
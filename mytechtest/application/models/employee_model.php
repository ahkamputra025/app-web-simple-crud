<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

    public function getDataEmployee(){
        $this->db->select('*');
        $this->db->from('tb_karyawan');
        $query = $this->db->get();
        return $query->result();
    }

    public function getMaxIdEmployee($table, $field){
        $this->db->select_max($field);
        return $this->db->get($table)->row_array()[$field];
    }

    public function InsertData($data){
        $this->db->insert('tb_karyawan', $data);
    }

    public function UpdateData($data, $code){
        $this->db->select('*');
        $this->db->from('tb_karyawan');
        $this->db->where('code', $code);
        $this->db->update('tb_karyawan', $data);
    }

    public function CekID($Id){
        $this->db->select('*');
        $this->db->from('tb_karyawan');
        $this->db->where('nomor_ktp', $Id);
        $query = $this->db->get();
        return $query->result();
    }

    public function GetData($code){
        $this->db->select('*');
        $this->db->from('tb_karyawan');
        $this->db->where('code', $code);
        $query = $this->db->get();
        return $query->result();
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('employee_model');
        $this->load->library('form_validation');
    }

	public function index(){
        $dataEmployee = $this->employee_model->getDataEmployee();
        $dt = array('dt_emp' => $dataEmployee);
		$this->load->view('dashboard_view', $dt);
	}

    public function AddData(){
        //Random string
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $file_name = str_shuffle($permitted_chars);

        // Upload image
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = $file_name;

        $this->load->library('upload', $config);
        //upload file foto ktp
        if ($this->upload->do_upload('foto_ktp')) { 
            $this->upload->data("file_name");
            $file = $this->upload->data('file_name');
        } else {
            $this->session->set_flashdata('errUploadFotoFlash', 'The photo format must be gif, jpg, png and jpeg');
            redirect(base_url());
        }
        //upload file foto profile
        if ($this->upload->do_upload('foto_profile')) {
            $this->upload->data("file_name");
            $file1 = $this->upload->data('file_name');
        } else {
            $this->session->set_flashdata('errUploadFotoFlash', 'The photo format must be gif, jpg, png and jpeg');
            redirect(base_url());
        }

        // Genereate Id
        $table = 'tb_karyawan';
        $field = 'id_karyawan';
        $lastId = $this->employee_model->getMaxIdEmployee($table, $field);
        $noIdEmployee = (int) substr($lastId, -3, 3);
        $noIdEmployee += 1;
        $newIdEmployee = 'KC'. sprintf('%03s', $noIdEmployee);

        $id_karyawan = $newIdEmployee;
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $gender = $this->input->post('gender');
        $foto_ktp = $file;
        $foto_profile = $file1;
        $nomor_ktp = $this->input->post('nomor_ktp');
        $status = $this->input->post('status');

        // cek id Card
        $dataEmployee = $this->employee_model->CekID($nomor_ktp);

        if(!$dataEmployee){
            
            $dtInsert = array(
                'id_karyawan' => $id_karyawan,
                'nama' => $nama,
                'alamat' => $alamat,
                'telp' => $telp,
                'gender' => $gender,
                'foto_ktp' => $foto_ktp,
                'foto_profile' => $foto_profile,
                'nomor_ktp' => $nomor_ktp,
                'status' => $status
            );

            $this->employee_model->InsertData($dtInsert);
            $this->session->set_flashdata('addFlash', 'Add data employee success');
            redirect(base_url());

        } else {
            $this->session->set_flashdata('cekIdFlash', 'Id Card already exist');
            redirect(base_url());
        }
    }

    public function UpdateDataText(){
        $code = $this->input->post('code');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $gender = $this->input->post('gender');
        $nomor_ktp = $this->input->post('nomor_ktp');

        //cek id Card
        $dataEmployee = $this->employee_model->CekID($nomor_ktp);
        foreach($dataEmployee as $row){
            $defaultCode = $row->code;
            $defaultNoKTP =  $row->nomor_ktp;
        }

        if(!$dataEmployee){
            $dtUpdate = array(
                'nama' => $nama,
                'alamat' => $alamat,
                'telp' => $telp,
                'gender' => $gender,
                'nomor_ktp' => $nomor_ktp
            );
            $this->employee_model->UpdateData($dtUpdate, $code);
            $this->session->set_flashdata('updateFlash', 'Update data success');
            redirect(base_url());
        } elseif($nomor_ktp == $defaultNoKTP && $defaultCode == $code ) {
            $dtUpdate = array(
                'nama' => $nama,
                'alamat' => $alamat,
                'telp' => $telp,
                'gender' => $gender,
            );
            $this->employee_model->UpdateData($dtUpdate, $code);
            $this->session->set_flashdata('updateFlash', 'Update data success');
            redirect(base_url());
        } else {
            $this->session->set_flashdata('cekIdFlash', 'Id Card already exist');
            redirect(base_url());
        }
    }

    public function SetStatus(){
        $code = $this->input->post('code');
        $status = $this->input->post('status');

        $dtSetStatus = array(
            'status' => $status,
        );

        $this->employee_model->UpdateData($dtSetStatus, $code);
        $this->session->set_flashdata('SetStatusFlash', 'Set Status success');
        redirect(base_url());
    }

    public function UpdateFotoKTP(){
        //Random string
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $file_name = str_shuffle($permitted_chars);

        // Upload image
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name']  = $file_name;

        $this->load->library('upload', $config);
        //upload file foto ktp
        if ($this->upload->do_upload('foto_ktp')) { 
            $this->upload->data("file_name");
            $file = $this->upload->data('file_name');
        } else {
            $this->session->set_flashdata('errUploadFotoFlash', 'The photo format must be gif, jpg, png and jpeg');
            redirect(base_url());
        }

        $code = $this->input->post('code');
        $foto_ktp = $file;
        $foto_ktp_old = $this->input->post('oldFotoKTP');

        $dtUdateFotoKTP = array(
            'foto_ktp' => $foto_ktp
        );

        $this->employee_model->UpdateData($dtUdateFotoKTP, $code);
        $this->session->set_flashdata('updateFotoFlash', 'Update photo success');

        //unlink foto
        unlink("./uploads/$foto_ktp_old");
        redirect(base_url());
    }

    public function UpdateFotoProfile(){
        //Random string
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $file_name = str_shuffle($permitted_chars);

        // Upload image
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name']  = $file_name;

        $this->load->library('upload', $config);
        //upload file foto ktp
        if ($this->upload->do_upload('foto_profile')) { 
            $this->upload->data("file_name");
            $file = $this->upload->data('file_name');
        } else {
            $this->session->set_flashdata('errUploadFotoFlash', 'The photo format must be gif, jpg, png and jpeg');
            redirect(base_url());
        }

        $code = $this->input->post('code');
        $foto_profile = $file;
        $foto_profile_old = $this->input->post('oldFotoProfile');

        $dtUdateFotoProfile = array(
            'foto_profile' => $foto_profile
        );

        $this->employee_model->UpdateData($dtUdateFotoProfile, $code);
        $this->session->set_flashdata('updateFotoFlash', 'Update photo success');

        //unlink foto
        unlink("./uploads/$foto_profile_old");
        redirect(base_url());   
    }
}
